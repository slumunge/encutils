package encutils

import (
	"fmt"
	"io/ioutil"
	"reflect"
	"testing"
)

func TestDecodeCellPermit(t *testing.T) {
	type args struct {
		permitString string
	}
	tests := []struct {
		name    string
		args    args
		want    CellPermitData
		wantErr bool
	}{
		{
			"Invalid Permit",
			args{"NO4D061320000830BEB9BFE3C7C6CE68B16411FD09F96982795C77B204F54D4"},
			CellPermitData{},
			true,
		},
		{
			"Valid Permit",
			args{"NO4D061320000830BEB9BFE3C7C6CE68B16411FD09F96982795C77B204F54D48"},
			CellPermitData{
				"NO4D0613",
				"20000830",
				"BEB9BFE3C7C6CE68",
				"B16411FD09F96982",
				"NO4D061320000830BEB9BFE3C7C6CE68B16411FD09F96982",
				"795C77B204F54D48",
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DecodeCellPermit(tt.args.permitString)
			if (err != nil) != tt.wantErr {
				t.Errorf("DecodeCellPermit() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DecodeCellPermit() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVerifyHwIdAndDecryptKeys(t *testing.T) {
	type args struct {
		cellPermitData CellPermitData
		hwIDASCII      string
	}
	tests := []struct {
		name    string
		args    args
		want    CellPermitKeys
		wantErr bool
	}{
		{
			"Valid HwId",
			args{CellPermitData{
				"NO4D0613",
				"20000830",
				"BEB9BFE3C7C6CE68",
				"B16411FD09F96982",
				"NO4D061320000830BEB9BFE3C7C6CE68B16411FD09F96982",
				"795C77B204F54D48",
			},
				"12348",
			},
			CellPermitKeys{"NO4D0613", []byte{193, 203, 81, 142, 156}, []byte{66, 21, 113, 204, 102}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := VerifyHwIdAndDecryptKeys(tt.args.cellPermitData, tt.args.hwIDASCII)
			if (err != nil) != tt.wantErr {
				t.Errorf("VerifyHwIdAndDecryptKeys() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("VerifyHwIdAndDecryptKeys() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecodePermit(t *testing.T) {
	permitFileData := `:DATE 20080809 11:11 
:VERSION 2 
:ENC 
NO4D061320000830BEB9BFE3C7C6CE68B16411FD09F96982795C77B204F54D48,0,5,PM,[Comment]
:ECS 
[List of licence cell permits for other vector products]
`
	_, err := DecodePermit(permitFileData, "12348")
	if err != nil {
		t.Errorf("TestDecodePermit() failed with %v", err)
	}

}

func TestDecryptS63Data(t *testing.T) {
	//read permit.txt
	permitFileData, err := ioutil.ReadFile("testdata/testdecrypt/7e/PERMIT.TXT") // just pass the file name
	if err != nil {
		fmt.Print(err)
		t.Errorf("TestDecryptS63Data() failed with %v", err)
	}
	permitData, err := DecodePermit(string(permitFileData), "12345")
	if err != nil {
		fmt.Print(err)
		t.Errorf("TestDecryptS63Data() failed with %v", err)
	}

	//read s63file
	encryptedData, err := ioutil.ReadFile("testdata/testdecrypt/7e/GB380720.000") // just pass the file name
	if err != nil {
		fmt.Print(err)
		t.Errorf("TestDecryptS63Data() failed with %v", err)
	}
	//decrypt file
	cellPermitKeys := permitData.cellPermitKeys["GB380720"]
	_, err = DecryptS63Data(encryptedData, cellPermitKeys)
	if err != nil {
		fmt.Print(err)
		t.Errorf("TestDecryptS63Data() failed with %v", err)
	}

}
