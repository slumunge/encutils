package encutils

import (
	"fmt"
	"io/fs"
	"os"
	"testing"
)

func TestDecryptAndCopy(t *testing.T) {
	//read permit.txt
	sourceDir := "testdata/testdecrypt/dirtest"
	destDir := "testdata/testdecrypt/dirtestoutput"
	hwIdList := []string{"12345"}

	err := DecryptAndCopy(sourceDir, destDir, hwIdList)
	if err != nil {
		fmt.Print(err)
		t.Errorf("TestDecryptAndCopy() failed with %v", err)
	}

}

func TestDecryptZipAndCopy(t *testing.T) {
	//read permit.txt
	sourceZip := "testdata/testdecrypt/ziptest/dirtest.zip"
	destZip := "testdata/testdecrypt/dirtestoutput/dirtestoutput.zip"
	tempDir := "testdata/testdecrypt/tmp"
	hwIdList := []string{"12345"}

	err := os.MkdirAll(tempDir, fs.ModePerm)
	if err != nil {
		fmt.Print(err)
		t.Errorf("TestDecryptZipAndCopy() failed with %v", err)
	}

	err = DecryptZipAndCopy(sourceZip, destZip, tempDir, hwIdList)
	if err != nil {
		fmt.Print(err)
		t.Errorf("TestDecryptZipAndCopy() failed with %v", err)
	}
	os.RemoveAll(tempDir)
	os.RemoveAll(destZip)

}
