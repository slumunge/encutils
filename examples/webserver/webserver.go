package main

import (
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"net/http"
	"os"

	"gitlab.com/slumunge/encutils"
)

// Compile templates on start of the application
var templates = `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Upload File</title>
  </head>
  <body>
    <form
      enctype="multipart/form-data"
      action="http://localhost:8080/upload"
      method="post"
    >
      <input type="file" name="myFile" />
	  <input type="text" name="hwid" />
      <input type="submit" value="upload" />
    </form>
  </body>
</html>
`

// Display the named template
func display(w http.ResponseWriter, page string, data interface{}) {
	io.WriteString(w, templates)
}

func decryptAndReturn(sourceZip string, hwId string) (string, error) {
	destZip := "/tmp/encutils/decryptedenc.zip"
	tempDir := "/tmp/encutils/tmpunzip/"
	hwIdList := []string{}
	hwIdList = append(hwIdList, hwId)

	err := os.MkdirAll(tempDir, fs.ModePerm)
	if err != nil {
		fmt.Print(err)
		return "", err
	}

	err = encutils.DecryptZipAndCopy(sourceZip, destZip, tempDir, hwIdList)
	if err != nil {
		fmt.Print(err)
		return "", err
	}
	os.RemoveAll(tempDir)

	return destZip, nil
}

func uploadFile(w http.ResponseWriter, r *http.Request) {
	// Maximum upload of 10 MB files
	r.ParseMultipartForm(10 << 20)

	hwId := r.FormValue("hwid")
	if hwId == "" {
		http.Error(w, "missing hardware id", http.StatusInternalServerError)
		return
	}

	// Get handler for filename, size and headers
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}

	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	// Create file
	dst, err := os.Create(handler.Filename)
	defer dst.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Copy the uploaded file to the created file on the filesystem
	if _, err := io.Copy(dst, file); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	dst.Close()

	newZip, err := decryptAndReturn(handler.Filename, "12345")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fileBytes, err := ioutil.ReadFile(newZip)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Write(fileBytes)
	os.RemoveAll(handler.Filename)
	os.RemoveAll(newZip)
	return
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		display(w, "upload", nil)
	case "POST":
		uploadFile(w, r)
	}
}

func main() {
	// Upload route
	http.HandleFunc("/", uploadHandler)

	//Listen on port 8080
	http.ListenAndServe(":8080", nil)
}
