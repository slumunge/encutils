module gitlab.com/slumunge/encutils

go 1.13

require (
	github.com/andreburgaud/crypt2go v0.13.0
	golang.org/x/crypto v0.0.0-20211202192323-5770296d904e
)
