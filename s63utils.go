package encutils

import (
	"archive/zip"
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"runtime"
	"strings"
	"sync"
)

type Dataset struct {
	date           string
	version        string
	cellPermits    map[string]CellPermitData
	cellPermitKeys map[string]CellPermitKeys
	hwId           string
}

func findFiles(directory string, pattern string) ([]string, error) {
	var files []string

	err := filepath.WalkDir(directory, func(path string, d fs.DirEntry, err error) error {

		if err != nil {

			fmt.Println(err)
			return nil
		}

		reg, err2 := regexp.Compile(pattern)

		if err2 != nil {

			return err2
		}

		if !d.IsDir() && reg.MatchString(d.Name()) {

			files = append(files, path)
		}

		return nil
	})

	if err != nil {
		return files, err
	}

	return files, nil

}

func copyFile(source string, destination string, mode fs.FileMode) error {

	bytesRead, err := ioutil.ReadFile(source)

	if err != nil {
		return err
	}

	err = ioutil.WriteFile(destination, bytesRead, mode)

	if err != nil {
		return err
	} else {
		return nil
	}
}

func getHwIds(from int, to int) []string {
	var hwIdList []string
	for i := from; i < to; i++ {
		hwIdAscii := fmt.Sprintf("%05X", i)
		hwIdList = append(hwIdList, hwIdAscii)
	}
	return hwIdList
}

func splitList(stringList []string, numberOfChunks int) [][]string {
	var divided [][]string

	chunkSize := (len(stringList) + numberOfChunks - 1) / numberOfChunks

	for i := 0; i < len(stringList); i += chunkSize {
		end := i + chunkSize

		if end > len(stringList) {
			end = len(stringList)
		}

		divided = append(divided, stringList[i:end])
	}
	return divided
}

//take in multiple hwids in case of multiple datasets with different keys
func DecryptAndCopy(sourceDirectory string, destinationDirectory string, hwIDASCII []string) error {

	var permitDataList []PermitData
	permitPaths, err := findFiles(sourceDirectory, "PERMIT\\.TXT")
	if err != nil {
		return errors.New("could not find permits")
	}

	for _, permitPath := range permitPaths {
		permitFileData, err := ioutil.ReadFile(permitPath) // just pass the file name
		if err != nil {
			fmt.Print(err)
			continue
		}

		numberOfWorkers := runtime.NumCPU()
		splitList := splitList(hwIDASCII, numberOfWorkers)

		//doing parallel for fun
		ctx := context.Background()
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		var wg sync.WaitGroup
		resCh := make(chan PermitData, 1)

		for _, currentList := range splitList {
			wg.Add(1)
			go func(hwIdList []string, resCh chan PermitData) {
				defer wg.Done()
				for _, hwId := range hwIdList {
					select {
					case <-ctx.Done():
						return
					default:
						permitData, err := DecodePermit(string(permitFileData), hwId)
						if err != nil || reflect.DeepEqual(permitData, PermitData{}) {
							continue
						} else {
							resCh <- permitData
							cancel()
							return
						}
					}

				}
			}(currentList, resCh)
		}
		wg.Wait()
		if errors.Is(ctx.Err(), context.Canceled) {
			permitDataList = append(permitDataList, <-resCh)
		}

	}

	if len(permitDataList) > 0 {
		//walk through everything and copy
		err := filepath.WalkDir(sourceDirectory, func(path string, d fs.DirEntry, err error) error {

			if err != nil {

				fmt.Println(err)
				return nil
			}

			reg, err2 := regexp.Compile(`[A-Z]{2}[0-9][A-Z0-9]{5}\.[0-9]{3}`)

			if err2 != nil {

				return err2
			}

			copyPath := strings.Replace(path, sourceDirectory, destinationDirectory, 1)
			fileInfo, err := d.Info()
			//just skip if error
			if err != nil {
				fmt.Println("no fileinfo found for ", path)
				return nil
			}

			if d.IsDir() {
				//create directory
				//fmt.Println("Creating directory ", copyPath)
				err := os.Mkdir(copyPath, fileInfo.Mode())
				if err != nil && !errors.Is(err, os.ErrExist) {
					return err
				}
			} else {
				if reg.MatchString(d.Name()) {
					//decrypt into new file
					//fmt.Println("Decrypting ", path)
					encryptedData, err := ioutil.ReadFile(path)
					if err != nil {
						fmt.Println("Failed reading file ", path)
					}
					//decrypt file
					decryptSuccess := false
					for _, currentPermitData := range permitDataList {
						cellPermitKeys, exists := currentPermitData.cellPermitKeys[strings.TrimSuffix(d.Name(), filepath.Ext(d.Name()))]
						if exists {
							decryptedData, err := DecryptS63Data(encryptedData, cellPermitKeys)
							if err != nil {
								continue
							} else {
								err = ioutil.WriteFile(copyPath, decryptedData, fileInfo.Mode())
								if err != nil {
									fmt.Println("Failed to write file ", copyPath, " with decrypted data")
									continue
								} else {
									decryptSuccess = true
								}
							}

						} else {
							continue
						}

					}
					if !decryptSuccess {
						//just copy file maybe
						//is it possible to check if it is an unecrypted s57 file maybe
						err = copyFile(path, copyPath, fileInfo.Mode())
						if err != nil {
							fmt.Println("failed to copy file ", path, " to ", copyPath)
						}

					}

				} else {
					//copy file
					//fmt.Println("Copying from ", path, " to ", copyPath)
					err = copyFile(path, copyPath, fileInfo.Mode())
				}
			}

			return nil
		})

		if err != nil {
			return err
		}
	} else {
		return errors.New("decrypt of permit failed or no permit files present")
	}

	return nil
}

func Unzip(src string, dest string) ([]string, error) {

	var filenames []string

	r, err := zip.OpenReader(src)
	if err != nil {
		return filenames, err
	}
	defer r.Close()

	for _, f := range r.File {

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dest, f.Name)

		// Check for ZipSlip. More Info: http://bit.ly/2MsjAWE
		if !strings.HasPrefix(fpath, filepath.Clean(dest)+string(os.PathSeparator)) {
			return filenames, fmt.Errorf("%s: illegal file path", fpath)
		}

		filenames = append(filenames, fpath)

		if f.FileInfo().IsDir() {
			// Make Folder
			os.MkdirAll(fpath, os.ModePerm)
			continue
		}

		// Make File
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
			return filenames, err
		}

		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return filenames, err
		}

		rc, err := f.Open()
		if err != nil {
			return filenames, err
		}

		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop
		outFile.Close()
		rc.Close()

		if err != nil {
			return filenames, err
		}
	}
	return filenames, nil
}

func ZipDirectory(src string, dst string, root string) error {
	file, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer file.Close()

	w := zip.NewWriter(file)
	defer w.Close()

	walker := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()

		// Ensure that `path` is not absolute; it should not start with "/".
		// This snippet happens to work because I don't use
		// absolute paths, but ensure your real-world code
		// transforms path into a zip-root relative path.
		relRootPath := strings.Replace(path, root, "./", 1)
		f, err := w.Create(relRootPath)
		if err != nil {
			return err
		}

		_, err = io.Copy(f, file)
		if err != nil {
			return err
		}

		return nil
	}
	err = filepath.Walk(src, walker)
	if err != nil {
		return err
	}
	return nil
}

func DecryptZipAndCopy(inputZipPath string, outputZipPath string, tempDir string, hwIDASCII []string) error {
	destDir := filepath.Join(tempDir, "/temps63unzip")
	err := os.MkdirAll(destDir, fs.ModePerm)
	if err != nil {
		return err
	}
	_, err = Unzip(inputZipPath, destDir)
	if err != nil {
		return err
	}

	unencryptedDir := filepath.Join(tempDir, "/temps57unzip")
	err = os.MkdirAll(unencryptedDir, fs.ModePerm)
	if err != nil {
		return err
	}

	err = DecryptAndCopy(destDir, unencryptedDir, hwIDASCII)
	if err != nil {
		return err
	}

	os.MkdirAll(filepath.Dir(outputZipPath), fs.ModePerm)
	err = ZipDirectory(unencryptedDir, outputZipPath, unencryptedDir)
	if err != nil {
		return err
	}

	return nil
}
