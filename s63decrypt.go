package encutils

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"strings"

	"bytes"
	"encoding/binary"
	"encoding/hex"
	"hash/crc32"

	"archive/zip"

	"github.com/andreburgaud/crypt2go/ecb"
	"golang.org/x/crypto/blowfish"
)

type PermitData struct {
	date           string
	version        string
	cellPermits    map[string]CellPermitData
	cellPermitKeys map[string]CellPermitKeys
	hwId           string
}

type CellPermitData struct {
	cellId            string
	expiryDate        string
	encryptedCellKey1 string
	encryptedCellKey2 string
	stringForChecksum string
	checkSumCrc       string
}
type CellPermitKeys struct {
	cellId              string
	unEncryptedCellKey1 []byte
	unEncryptedCellKey2 []byte
}

func pad(in []byte, blockSize int) []byte {
	padding := blockSize - (len(in) % blockSize)
	if padding == 0 {
		padding = blockSize
	}
	for i := 0; i < padding; i++ {
		in = append(in, byte(padding))
	}
	return in
}

func DecodePermit(permitFileData string, hwIDASCII string) (PermitData, error) {

	permitData := PermitData{}
	permitData.hwId = hwIDASCII

	scanner := bufio.NewScanner(strings.NewReader(permitFileData))
	for scanner.Scan() {
		lineText := scanner.Text()
		if strings.HasPrefix(lineText, ":DATE") {
			permitData.date = lineText[len(":DATE")+1 : len(lineText)-1]
		} else if strings.HasPrefix(lineText, ":VERSION") {
			permitData.version = lineText[len(":VERSION")+1 : len(lineText)-1]
		} else if strings.HasPrefix(lineText, ":ENC") {
			permitData.cellPermits = make(map[string]CellPermitData)
			permitData.cellPermitKeys = make(map[string]CellPermitKeys)
			for scanner.Scan() {
				lineText = scanner.Text()
				if !strings.HasPrefix(lineText, ":") {
					cellPermitText := lineText[:64]
					cellPermitData, err := DecodeCellPermit(cellPermitText)
					if err == nil {
						cellPermitKeys, err := VerifyHwIdAndDecryptKeys(cellPermitData, hwIDASCII)
						if err == nil {
							permitData.cellPermits[cellPermitData.cellId] = cellPermitData
							permitData.cellPermitKeys[cellPermitData.cellId] = cellPermitKeys
						} else {
							//fmt.Println(err)
							//probably incorrect hwid...
							return PermitData{}, err
						}
					} else {
						fmt.Println(err)
						//probably incorrect string or something so just continue

					}
				} else {
					break
				}
			}
		}
	}

	return permitData, nil

}

func DecodeCellPermit(permitString string) (CellPermitData, error) {

	if len(permitString) != 64 {
		return CellPermitData{}, errors.New("incorrect permit string")
	}

	cellPermitData := CellPermitData{}
	cellPermitData.cellId = permitString[0:8]
	cellPermitData.expiryDate = permitString[8:16]
	cellPermitData.encryptedCellKey1 = permitString[16:32]
	cellPermitData.encryptedCellKey2 = permitString[32:48]
	cellPermitData.checkSumCrc = permitString[48:64]
	cellPermitData.stringForChecksum = permitString[0:48]

	return cellPermitData, nil

}

func VerifyHwIdAndDecryptKeys(cellPermitData CellPermitData, hwIDASCII string) (CellPermitKeys, error) {

	//hwid is 5 digit hexadecimal number... gives 16^5 different possibilities. how hard is that to bruteforce
	//create hwid encryption/decryption key by appending first byte
	hwIdBytes := []byte(hwIDASCII)
	hwIdBytes = append(hwIdBytes, hwIdBytes[0])

	checkSumBytes, _ := hex.DecodeString(cellPermitData.checkSumCrc)

	bytesToCalcChecksum := []byte(cellPermitData.stringForChecksum)
	checkSumCalc := crc32.ChecksumIEEE(bytesToCalcChecksum)

	//do encrypt
	blow, _ := blowfish.NewCipher(hwIdBytes)

	enc := ecb.NewECBEncrypter(blow)
	encrypted := make([]byte, 8)
	unencrypted := make([]byte, 4)

	binary.BigEndian.PutUint32(unencrypted, checkSumCalc)
	unencrypted = pad(unencrypted, blowfish.BlockSize)

	enc.CryptBlocks(encrypted, unencrypted)

	if bytes.Equal(encrypted[0:4], checkSumBytes[0:4]) {
		cellPermitKeys := CellPermitKeys{}
		cellPermitKeys.cellId = cellPermitData.cellId
		//fmt.Println("Found matching hw id: ", hwIDASCII)
		dec := ecb.NewECBDecrypter(blow)
		encryptedDataBytes, err := hex.DecodeString(cellPermitData.encryptedCellKey1)

		if err != nil {
			return CellPermitKeys{}, errors.New("failed to hex decode encryptedcellkey1")
		}
		unEncryptedDataBytes := make([]byte, len(encryptedDataBytes))
		dec.CryptBlocks(unEncryptedDataBytes, encryptedDataBytes)
		cellPermitKeys.unEncryptedCellKey1 = unEncryptedDataBytes[0:5]

		encryptedDataBytes, err = hex.DecodeString(cellPermitData.encryptedCellKey2)
		if err != nil {
			return CellPermitKeys{}, errors.New("failed to hex decode encryptedcellkey2")
		}
		unEncryptedDataBytes = make([]byte, len(encryptedDataBytes))
		dec.CryptBlocks(unEncryptedDataBytes, encryptedDataBytes)
		cellPermitKeys.unEncryptedCellKey2 = unEncryptedDataBytes[0:5]
		return cellPermitKeys, nil
	} else {
		return CellPermitKeys{}, errors.New("incorrect checksum")
	}

}

func DecryptS63Data(s63Data []byte, cellPermitKeys CellPermitKeys) ([]byte, error) {

	keys := []([]byte){cellPermitKeys.unEncryptedCellKey1,
		cellPermitKeys.unEncryptedCellKey2,
	}
	for _, decryptKey := range keys {
		blow, _ := blowfish.NewCipher(decryptKey)
		dec := ecb.NewECBDecrypter(blow)
		encryptedDataPadded := s63Data
		unEncryptedDataBytes := make([]byte, len(encryptedDataPadded))
		dec.CryptBlocks(unEncryptedDataBytes, encryptedDataPadded)
		//fmt.Print(string(unEncryptedDataBytes[0:20]))

		zipReader, err := zip.NewReader(bytes.NewReader(unEncryptedDataBytes), int64(len(unEncryptedDataBytes)))
		if err != nil {
			fmt.Print(err)
			continue
		}
		zipFile := zipReader.File[0]
		f, err := zipFile.Open()
		if err != nil {
			continue
		}
		defer f.Close()
		return ioutil.ReadAll(f)
	}

	return nil, errors.New("failed decrypting data")

}
